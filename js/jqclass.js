/**
 * Class jqClass
 *
 * Provides a class functionality for jQuery
 *
 * @copyright  	Copyright (C) 2018 Daniel Nemeth
 * @author		Daniel Nemeth <office@daniel-nemeth.com>
 *
 * Class declaration:
 *
 * 		var myClass = new jqClass({
 *			options: {
 *				content: null,
 *				container: null
 *			},
 *
 *			initialize: function(options) {
 *				this.setOptions(options);
 *			},
 *
 *			speak: function() {
 *				this.options.container.html(
 *					'<b>' + this.options.content + '</b>'
 *				);
 *			}
 *		});
 *
 * Usage:
 *
 * 		var mytest = new myClass({
 *			content: 'foo',
 *			container: $('#bar')
 *		});
 *		mytest.speak();
 */
var jqClass;

(function(j) {
	var Options = {
		setOptions: function(options) {
			j.extend(true, this.options, options);
		}
	};

	jqClass = function (params)
	{
		if (params instanceof Function) {
			params = {
				initialize: params
			};
		}

		var instance = function() {
			j.extend(true, this, params);
			j.extend(true, this, Options);

			if (this.initialize instanceof Function) {
				this.initialize.apply(this, arguments);
			}

			return this;
		}

		j.extend(true, instance, this);
		return instance;
	};
})(jQuery);
