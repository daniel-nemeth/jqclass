# jQuery class

Provides an ordinary class functionality for jQuery. This is an contao-component.


## Class declaration
 
```javascript
var myClass = new jqClass({
    options: {
        content: null,
        container: null
    },

    initialize: function(options) {
        this.setOptions(options);
    },
 
    speak: function() {
        this.options.container.html(
            '<b>' + this.options.content + '</b>'
        );
    }
});
```

## Usage

```javascript
var mytest = new myClass({
    content: 'foo',
    container: $('#bar')
});

mytest.speak();
```